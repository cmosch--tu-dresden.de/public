# VNC Setup
Virtuelle Maschine der Research Cloud mit:
* Ubuntu Server 20.04 LTS
* XFCE Desktop Environment
* TurboVNC Server
* NoVNC Client


## XFCE Desktop Environment
Installation:
```
$ sudo apt install xfce4
```
Während der Installation wird gefragt, welcher Display Manager installiert
werden soll. Ich habe mich für `lightdm` entschieden. Eigentlich brauchen wir
keinen Display Manager, weil wir die Desktop-Sessions selbst starten.

Display Manager deaktivieren:
```
$ sudo systemctl disable lightdm
```


## TurboVNC Server
Download des Pakets (`wget` installieren, falls nicht vorhanden):
```
$ wget https://sourceforge.net/projects/turbovnc/files/2.2.5/turbovnc_2.2.5_amd64.deb/download -O turbovnc_2.2.5_amd64.deb
```

Installation:
```
$ sudo dpkg -i turbovnc_2.2.5_amd64.deb
```

PATH erweitern. Dazu folgende Zeile in `~/.bash_profile` eintragen:
```
export PATH="${PATH}:/opt/TurboVNC/bin"
```
und danach neu laden:
```
$ source ~/.bash_profile
```

Passwortdatei hinterlegen:
```
$ vncpasswd .vnctoken
```
Hier wird ein Passwort interaktiv abgefragt. Maximale Passwortlänge ist auf
8 Zeichen begrenzt.

TurboVNC Server starten:
```
$ vncserver :1 -rfbauth .vnctoken -xstartup startxfce4
```

Derzeit laufende Server auflisten:
```
$ vncserver -list
```


## NoVNC Client
Download:
```
$ wget https://github.com/novnc/noVNC/archive/v1.2.0.tar.gz -O noVNC-v1.2.0.tar.gz
```

Entpacken:
```
$ tar -zxvf noVNC-v1.2.0.tar.gz
```

Starten:
```
./noVNC-1.2.0/utils/launch.sh --vnc localhost:5901
```


## Software

### LAMMPS
[...]

### OVITO
[...]


## Sonstiges

### Terminal im Desktop reparieren
In meinem Test funktionierte das Standard-Terminal nicht, weil es wohl auf einen
Terminal Emulator eingestellt ist, der nicht installiert ist. Dies reparieren
folgende Schritte:

Terminal Emulator installieren:
```
$ sudo apt install xfce4-terminal
```

Auf dem Desktop muss man nun das Einstellungsmenü für Standardanwendungen
öffnen. Dazu klickt man auf
*Applications (Button im Panel oben links) > Settings > Preferred Applications*.
Dort auf den Tab *Utilities* gehen und unter *Terminal Emulator* das
*Xfce Terminal* auswählen. Nach einem Klick auf *Close* schließt sich das
Fenster und das Standard-Terminal funktioniert wieder.
